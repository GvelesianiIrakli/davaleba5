package com.example.Davaleba5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        logInButton.setOnClickListener {
            authorisation()
        }
    }

    private fun authorisation() {
        val email: String = emailEditText.text.toString()
        val password: String = passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()){
            val intent = Intent (this, SecondActivity::class.java)
            startActivity(intent)

        }
        else{
            Toast.makeText(this, "fill all lines", Toast.LENGTH_LONG).show()
        }
    }
}