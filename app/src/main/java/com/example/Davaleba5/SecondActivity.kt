package com.example.Davaleba5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init(){
        Glide.with(this)
                .load("https://www.denofgeek.com/wp-content/uploads/2019/05/game_of_thrones_season_8_dragon_deaths_daenerys.jpg?resize=768%2C432")
                .placeholder(R.mipmap.ic_launcher_round).into(coverImageView)

        Glide.with(this)
                .load("https://www.denofgeek.com/wp-content/uploads/2016/05/game_of_thrones_season_8_daenerys_targaryen_predictions-scaled.jpg?resize=768%2C432")
                .placeholder(R.mipmap.ic_launcher_round).into(profileImageView)


    }

}